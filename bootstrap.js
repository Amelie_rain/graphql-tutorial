const { readdir } = require('fs');
const { promisify } = require('util');
const { basename } = require('path');

exports.bootstrap = async function(server) {
  // Bootstrap
  console.log('Starting bootstrap');

  const files = await promisify(readdir)('./models');

  for (const file of files) {
    // Check if is js file
    if (!/\.js$/.test(file)) continue;

    const Model = require(`./models/${file}`);
    if (Model === null) continue;

    const endName = basename(file, '.js');
    console.log(`Boot ${endName}`);

    // Get alls
    server.route({
      method: 'GET',
      path: `/api/${endName}`,
      handler: () => Model.find()
    });

    // Get by id
    server.route({
      method: 'GET',
      path: `/api/${endName}/{id}`,
      handler: ({ params }) => Model.findById(params.id)
    });

    // Count
    server.route({
      method: 'GET',
      path: `/api/${endName}/count`,
      handler: ({ params }) => Model.countDocuments()
    });

    // Create
    server.route({
      method: 'POST',
      path: `/api/${endName}`,
      handler: ({ payload }) => Model.create(payload)
    });

    // DeleteAll
    server.route({
      method: 'DELETE',
      path: `/api/${endName}`,
      handler: () => Model.remove()
    });

    // DeleteById
    server.route({
      method: 'DELETE',
      path: `/api/${endName}/{id}`,
      handler: ({ params }) => Model.remove(params.id)
    });

    // Update
    server.route({
      method: 'PUT',
      path: `api/${endName}/{id}`,
      handler: ({ params, payload }) => Model.update(Model.findById(params.id), payload)
    });
  }
};
