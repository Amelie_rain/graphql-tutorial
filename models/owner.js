const mongoose = require('mongoose');

module.exports = mongoose.model('owner', {
  name: {
    type: String,
    minlength: 5,
    maxlength: 20,
    unique: true,
    required: true
  }
});
