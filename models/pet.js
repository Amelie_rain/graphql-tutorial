const mongoose = require('mongoose');

module.exports = mongoose.model('pet', {
  type: {
    type: String,
    enum: ['dog', 'horse'],
    required: true
  },
  name: {
    type: String,
    minlength: 2,
    maxlength: 12,
    unique: true,
    required: true
  }
});
