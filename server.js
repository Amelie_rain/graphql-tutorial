const Hapi = require('hapi');
const mongoose = require('mongoose');
const { bootstrap } = require('./bootstrap');

mongoose.Promise = global.Promise;

// Main function
(async () => {
  // Connect to mongodb
  await mongoose.connect(
    'mongodb://localhost:5050/test',
    {
      useNewUrlParser: true
    }
  );
  console.log('Connection success');

  // Create demo document
  // const Lola = new Pet({
  //   type: 'dog',
  //   name: 'Lola'
  // });
  // await Lola.save();
  // console.log('Create demo document');

  // Server
  const server = new Hapi.server({ port: 4200 });

  // route
  server.route({
    method: 'GET',
    path: '/',
    handler: () => 'Welcome!'
  });

  // Bootstrap
  await bootstrap(server);

  await server.start();
  console.log(`Server ready on ${server.info.uri}`);
})();
